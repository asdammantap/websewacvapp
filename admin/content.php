<?php
include "../config/koneksi.php";
include "../config/library.php";
include "../config/fungsi_indotgl.php";
include "../config/fungsi_combobox.php";
include "../config/class_paging.php";
include "../config/fungsi_rupiah.php";

// Bagian Home
if ($_GET[module]=='home'){
 
  echo "<h2>Selamat Datang</h2>
          <p>Hai <b>$_SESSION[namalengkap]</b>, selamat datang di halaman Administrator.<br> Silahkan klik menu pilihan yang berada 
          di bawah header untuk mengelola website. </p>
          <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
          <p align=right>Login : $hari_ini, ";
  echo tgl_indo(date("Y m d")); 
  echo " | "; 
  echo date("H:i:s");
  echo " WIB</p>";
  
}

// Bagian Modul
elseif ($_GET[module]=='modul'){

    include "modul/mod_modul/modul.php";
 
}

// // Bagian Kategori
// elseif ($_GET[module]=='jenis'){
  // if ($_SESSION['leveluser']=='admin'){
    // include "modul/mod_jenis/jenis.php";
  // }
// }

// Bagian alat
elseif ($_GET[module]=='alat'){
 
    include "modul/mod_alat/alat.php";
  
}

// Bagian Time Sheet
elseif ($_GET[module]=='timesheet'){
 
    include "modul/mod_timesheet/timesheet.php";
  
}

// Bagian user
elseif ($_GET[module]=='user'){
 
    include "modul/mod_user/user.php";
  
}

// Bagian Mobil
elseif ($_GET[module]=='mobil'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_mobil/mobil.php";
  }
}

// Bagian penyewa
elseif ($_GET[module]=='penyewa'){
  
    include "modul/mod_penyewa/penyewa.php";
 
}


// Bagian Order
elseif ($_GET[module]=='order'){
 
    include "modul/mod_order/order.php";

}

// Bagian Profil
elseif ($_GET[module]=='profil'){
  if ($_SESSION['leveluser']=='admin'){
    include "modul/mod_profil/profil.php";
  }
}

// Bagian Order
elseif ($_GET[module]=='hubungi'){
 
    include "modul/mod_hubungi/hubungi.php";

}

// Bagian Cara Pembelian
elseif ($_GET[module]=='carabeli'){
  
    include "modul/mod_carabeli/carabeli.php";
  
}



// Bagian Kota/Ongkos Kirim
elseif ($_GET[module]=='ongkoskirim'){
  
    include "modul/mod_ongkoskirim/ongkoskirim.php";
  
}

// Bagian Password
elseif ($_GET[module]=='password'){
  
    include "modul/mod_password/password.php";
  
}

// Bagian Laporan
elseif ($_GET[module]=='laporan'){
  
    include "modul/mod_laporan/lapsewaall.php";
  
}


// Apabila modul tidak ditemukan
else{
  echo "<p><b>MODUL BELUM ADA ATAU BELUM LENGKAP</b></p>";
}
?>
