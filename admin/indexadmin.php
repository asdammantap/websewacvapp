<?php
	  session_start();
	  if(!isset($_SESSION['namauser'])) {
	  header('location:index.php'); }
	  else { $usr = $_SESSION['namauser']; }
	  require_once("../config/koneksi.php");
	  
	  $query = mysql_query("SELECT * FROM pengguna WHERE namauser = '$usr'");
	  $hasil = mysql_fetch_array($query);
	?>
<html>
<head>
<title>Ruang Administrator</title>
<script type="text/javascript">
    //set timezone
    <?php date_default_timezone_set('Asia/Jakarta'); ?>
    //buat object date berdasarkan waktu di server
    var serverTime = new Date(<?php print date('Y, m, d, H, i, s, 0'); ?>);
    //buat object date berdasarkan waktu di client
    var clientTime = new Date();
    //hitung selisih
    var Diff = serverTime.getTime() - clientTime.getTime();    
    //fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
    function displayServerTime(){
        //buat object date berdasarkan waktu di client
        var clientTime = new Date();
        //buat object date dengan menghitung selisih waktu client dan server
        var time = new Date(clientTime.getTime() + Diff);
        //ambil nilai jam
        var sh = time.getHours().toString();
        //ambil nilai menit
        var sm = time.getMinutes().toString();
        //ambil nilai detik
        var ss = time.getSeconds().toString();
        //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
        document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" +(sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
    }
</script>
<link href="css/bootstrap.css" rel="stylesheet"> 
<!-- <link href="css/custom.css" rel="stylesheet">  -->

</head>
<body onLoad="setInterval('displayServerTime()', 1000);">

<div class="container">
<div class="head">
<center>
<h1>CV.ARI PUTRA PRATAMA</h1>
<h3>SUPPLIER DAN PERDAGANGAN UMUM</h3>
</center>
</div>
<div class="navbar">
                <div class="navbar-inner">
                    <ul class="nav">   
                        <li>
                            <a href="indexadmin.php"><i class="icon-home"></i>Beranda</a>
                        </li>                        
                        <li>
                            <a href="inputinfo.php">Alat</a>                                
                        </li>
                        <li>
                            <a href="inputevaluasi.php">Input Evaluasi</a> 
                        </li>
						<li>
							<a href="uploadgallery.php">Input Galeri</a>
						</li>
						<li>
							<a href="uploadmateri.php">Upload Materi</a>
						</li>
						<li>
							<a href="logout.php">LOGOUT</a>
						</li>
                    </ul>
                </div>
            </div>
			<div class="span7">
				<h3>SELAMAT DATANG ADMIN</h3>
			</div>  
			<div class="span4">
			<div class="navbar-form">
			<h4><span id="date"><?php print date('d F Y'); ?></span></h4>
			<h1><span id="clock"><?php print date('H:i:s'); ?></span></h1>
			</div>
			</div>
		</div>
		<div class="modal-footer"><small>JLN.K.H. HASYM LINKG. KERAMAT RT/RW 10/05 KEL. TEGALRATU KEC. CIWANDAN CILEGON-BANTEN</small></div> 
</div>
</body>
<html>
