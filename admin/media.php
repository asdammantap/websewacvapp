<?php
  error_reporting(0);

session_start();

if (empty($_SESSION[namauser]) AND empty($_SESSION[pass])){
  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=index.php><b>LOGIN</b></a></center>";
}
else{
?>

<html>
<head>
<title></title>
<link href="style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>
<div id="header">
	 <div class="navbar">
                <div class="navbar-inner">
                    <div class="nav-collapse collapse" style="margin-top:5px;">
              <ul class="nav">
                     <li><a href="media.php?module=home">Beranda</a></li>
                
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Input Data<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="media.php?module=alat">Alat</a></li>
                    <li><a href="media.php?module=user">Pengguna</a></li>
                    <li><a href="media.php?module=ongkoskirim">Ongkos Kirim</a></li>
                    <li><a href="media.php?module=timesheet">Timesheet</a></li>
                  </ul>
                </li>
				 <li><a href="modul/mod_laporan/lapsewaall.php">Laporan Penyewaan</a></li>
				<li>
							<a href="logout.php">LOGOUT</a>
						</li>
              </ul>
            </div><!--/.nav-collapse -->
                </div>
            </div>

  <div id="content">
		<?php include "content.php"; ?>
  </div>
  
		<div id="footer">
			JLN.K.H. HASYM LINKG. KERAMAT RT/RW 10/05 KEL. TEGALRATU KEC. CIWANDAN CILEGON-BANTEN
		</div>
</div>

   <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>


</body>
</html>
<?php
}
?>
