-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2015 at 08:51 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cvapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `namalengkap` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `namauser` varchar(20) NOT NULL DEFAULT '',
  `pass` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`namalengkap`, `alamat`, `email`, `namauser`, `pass`) VALUES
('1', '1', '1', '1', '1'),
('admin', 'serang', 'admin@aja.com', 'Admin', 'admin'),
('Admin', 'dahdhakjdak', 'asal', 'e', '123'),
('q', 'q', 'q', 'q', 'q');

-- --------------------------------------------------------

--
-- Stand-in structure for view `query_penyewaan`
--
CREATE TABLE IF NOT EXISTS `query_penyewaan` (
`no_nota` varchar(10)
,`tgl_sewa` date
,`kode_alat` varchar(10)
,`nama_alat` varchar(20)
,`jenis_alat` varchar(15)
,`banyak` int(5)
);
-- --------------------------------------------------------

--
-- Table structure for table `t_alat`
--

CREATE TABLE IF NOT EXISTS `t_alat` (
  `kode_alat` varchar(10) NOT NULL,
  `nama_alat` varchar(20) NOT NULL,
  `jenis_alat` varchar(15) NOT NULL,
  `deskripsi` text,
  `harga_sewa` int(10) NOT NULL,
  `stok` int(3) NOT NULL,
  `gambar` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_alat`
--

INSERT INTO `t_alat` (`kode_alat`, `nama_alat`, `jenis_alat`, `deskripsi`, `harga_sewa`, `stok`, `gambar`) VALUES
('A001', 'Kobelco FK', 'Kobelco', 'Alat Yang Digunakan Untuk Mengeruk', 200000, 100, '81IMG_20150626_052311.jpg'),
('A002', 'Kobelco S008', 'Kobelco', 'Alat Yang Digunakan Untuk Mengeruk', 250000, 100, '54IMG_20150626_052333.jpg'),
('A003', 'Kobelco Extra', 'Kobelco', 'Alat Yang Digunakan Untuk Mengeruk', 350000, 100, '49IMG_20150626_052344.jpg'),
('A004', 'Wadah Kayu', 'Pelengkap', 'Alat Yang Digunakan Untuk Menaruh Peralatan Kecil', 100000, 100, '26IMG_20150626_052412.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_nota_sewa`
--

CREATE TABLE IF NOT EXISTS `t_nota_sewa` (
  `no_surat_jalan` varchar(10) NOT NULL,
  `no_nota` varchar(10) NOT NULL,
  `tgl_sewa` date NOT NULL,
  `kode_penyewa` varchar(10) NOT NULL,
  `id_kota` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_nota_sewa`
--

INSERT INTO `t_nota_sewa` (`no_surat_jalan`, `no_nota`, `tgl_sewa`, `kode_penyewa`, `id_kota`) VALUES
('SJ001', 'N001', '2015-06-26', 'P001', 'D001');

-- --------------------------------------------------------

--
-- Table structure for table `t_nota_sewa_detail`
--

CREATE TABLE IF NOT EXISTS `t_nota_sewa_detail` (
  `no_nota` varchar(10) NOT NULL,
  `kode_alat` varchar(10) NOT NULL,
  `banyak` int(5) NOT NULL,
  `harga` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_nota_sewa_detail`
--

INSERT INTO `t_nota_sewa_detail` (`no_nota`, `kode_alat`, `banyak`, `harga`) VALUES
('N001', 'tyuuyi', 1, 1909000);

-- --------------------------------------------------------

--
-- Table structure for table `t_nota_sewa_temp`
--

CREATE TABLE IF NOT EXISTS `t_nota_sewa_temp` (
`no_notasewasementara` int(10) NOT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `kode_alat` varchar(10) DEFAULT NULL,
  `banyak` int(5) DEFAULT NULL,
  `tgl_sewasementara` date DEFAULT NULL,
  `stok_sementara` int(3) DEFAULT NULL,
  `harga_sewa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_ongkir`
--

CREATE TABLE IF NOT EXISTS `t_ongkir` (
  `id_kota` varchar(10) NOT NULL,
  `kota` varchar(20) NOT NULL,
  `nominal` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ongkir`
--

INSERT INTO `t_ongkir` (`id_kota`, `kota`, `nominal`) VALUES
('B001', 'Bandung', 10000),
('D001', 'Depok', 20000);

-- --------------------------------------------------------

--
-- Table structure for table `t_penyewa`
--

CREATE TABLE IF NOT EXISTS `t_penyewa` (
  `kode_penyewa` varchar(10) NOT NULL,
  `nama_penyewa` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_kota` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_penyewa`
--

INSERT INTO `t_penyewa` (`kode_penyewa`, `nama_penyewa`, `alamat`, `no_hp`, `email`, `password`, `id_kota`) VALUES
('P001', 'asal', 'serang', '083718', 'asal@gmail.com', '123', 'D001'),
('P002', 'sadam', 'puncak', '0887634', 'asdamgrimson@gmail.com', '1231', 'D001'),
('P003', 'indy', 'serang', '0887467', 'asal2@gmail.com', '123', 'B001'),
('P004', 'rika', 'serang', '088737', 'as1605dam@gmail.com', '234', 'D001'),
('P005', 'daeni', 'serang', '08778378', 'as3605dam@gmail.com', '123', 'D001'),
('P006', 'zaenab', 'serang', '087831831', 'asdamgrimson1@gmail.com', '123', 'B001'),
('P007', 'zainwl', 'serang', '0839289', 'asal65@gmail.com', 'asdan', 'B001'),
('P008', 'hjjada', 'seradan', '093829', 'as5305dam@gmail.com', '2819', 'B001'),
('P009', 'uirui', 'uiui', '0898', 'as9605dam@gmail.com', '890', 'D001'),
('P010', 'ioeioro', 'seradan', '085954', 'as1111dam@gmail.com', '123', 'B001'),
('P011', 'tyrop', 'serang', '0874827', 'as2222dam@gmail.com', '345', 'B001'),
('P012', 'itpo', 'hdsjui', '08989', 'as5555dam@gmail.com', '90', 'B001'),
('P013', 'ppppp', 'jdskj', '0898', 'asa583l@gmail.com', '90', 'B001'),
('P014', 'papopaop', 'hjahj', '089898', 'as0009dam@gmail.com', '7382', 'B001'),
('P015', 'uirio', 'hhjhjh', '098989', 'as65747dam@gmail.com', 'jkjkj', 'B001'),
('P016', 'uirio', 'hhjhjh', '098989', 'as65747dam@gmail.com', 'jkjkj', 'B001'),
('P017', 'sahjhj', 'jkjk', '0809', 'as1605sam@gmail.com', 'ioiio', 'B001'),
('P018', 'ioreio', 'ajkjk', '0889', 'as2605wam@gmail.com', '90', 'B001'),
('P019', 'HDAJHD', 'JUIU', '0898', 'as1119dam@gmail.com', 'IIO', 'B001'),
('P020', 'hdauuyu', 'jkjkjk', '089778', 'as905dam@gmail.com', 'ioio', 'B001'),
('P021', 'hjah', 'jkjk', '08989', 'as0009sadam@gmail.com', '9080', 'B001'),
('P022', 'djkajk', 'jkjk', '08998', 'as12345dam@gmail.com', 'uiui', 'B001'),
('P023', 'hjhjhjhj', 'tuyu', '07868', 'as999dam@gmail.com', 'uiuiu', 'B001'),
('P024', 'hjhjhjhj', 'tuyu', '07868', 'as999dam@gmail.com', 'uiuiu', 'B001'),
('P025', 'iiop', 'KJKJ', '089898', 'masal@gmail.com', 'JKJK', 'B001'),
('P026', 'lko', 'lklk', '08978', 'gaasal@gmail.com', 'opop', 'B001'),
('P027', 'yiii', 'uio', '08767', 'as345dam@gmail.com', 'kjk', 'B001'),
('P028', 'yiii', 'uio', '08767', 'as345dam@gmail.com', 'kjk', 'B001'),
('P029', 'hdajhdja', 'suuuyy', '08989878', 'asal@gmail.com', '898989', 'B001'),
('P030', 'idoaip', 'jjkkjk', '08998989', 'as9605dam@gmail.com', 'ioi', 'B001'),
('P031', 'ioioi', 'jkjk', '09090', 'asal9@gmail.com', '18989', 'B001'),
('P032', 'iuiui', 'ddfdf', '098877', 'asam@gmail.com', 'wewe', 'B001'),
('P033', 'tytyy', 'rtrt', '098787', 'asas@gmail.com', 'qqwqw', 'B001'),
('P034', 'opoio', 'dffdfd', '0998988', 'masasal@gmail.com', 'qwqwq', 'B001'),
('P035', 'pppppui', 'dsfdf', '08644', 'jasal@gmail.com', 'qwqw', 'B001'),
('P036', 'yuyuy', 'rtyty', '23232', 'zasal@gmail.com', 'qwqw', 'D001'),
('P037', 'tyytty', 'erer', '978867', 'sd@sal.com', 'qwqwq', 'B001'),
('P038', 'hjhj', 'reer', '089786', 'w@sdal.com', 'qwqw', 'B001'),
('P039', 'kl', 'gh', '6767', 'pasal@gmail.com', 'qw', 'B001'),
('P040', 'jkjk', 'fgfg', '45454', 'klasal@gmail.com', 'wewe', 'B001'),
('P041', 'klklk', 'hjhj', '0877676', 'plasasal@gmail.com', 'jkjk', 'B001'),
('P042', 'uiuiui', 'qwqw', '23232', 'sdm@dal.com', 'qwqw', 'B001');

-- --------------------------------------------------------

--
-- Table structure for table `t_timesheet`
--

CREATE TABLE IF NOT EXISTS `t_timesheet` (
  `no_nota` varchar(10) NOT NULL,
  `kode_alat` varchar(10) NOT NULL,
  `tgl_kerja` date NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `total_jam` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_timesheet`
--

INSERT INTO `t_timesheet` (`no_nota`, `kode_alat`, `tgl_kerja`, `jam_mulai`, `jam_selesai`, `total_jam`) VALUES
('N001', 'poipoiu', '2015-10-10', '11:00:00', '14:00:00', 3);

-- --------------------------------------------------------

--
-- Structure for view `query_penyewaan`
--
DROP TABLE IF EXISTS `query_penyewaan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_penyewaan` AS select `sd`.`no_nota` AS `no_nota`,`s`.`tgl_sewa` AS `tgl_sewa`,`sd`.`kode_alat` AS `kode_alat`,`a`.`nama_alat` AS `nama_alat`,`a`.`jenis_alat` AS `jenis_alat`,`sd`.`banyak` AS `banyak` from ((`t_nota_sewa` `s` join `t_nota_sewa_detail` `sd`) join `t_alat` `a`) where ((`sd`.`no_nota` = `s`.`no_nota`) and (`sd`.`kode_alat` = `a`.`kode_alat`));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
 ADD PRIMARY KEY (`namauser`);

--
-- Indexes for table `t_alat`
--
ALTER TABLE `t_alat`
 ADD PRIMARY KEY (`kode_alat`);

--
-- Indexes for table `t_nota_sewa`
--
ALTER TABLE `t_nota_sewa`
 ADD PRIMARY KEY (`no_nota`), ADD UNIQUE KEY `kode_penyewa` (`kode_penyewa`), ADD UNIQUE KEY `no_nota` (`no_nota`), ADD UNIQUE KEY `no_surat_jalan` (`no_surat_jalan`);

--
-- Indexes for table `t_nota_sewa_detail`
--
ALTER TABLE `t_nota_sewa_detail`
 ADD PRIMARY KEY (`no_nota`,`kode_alat`);

--
-- Indexes for table `t_nota_sewa_temp`
--
ALTER TABLE `t_nota_sewa_temp`
 ADD PRIMARY KEY (`no_notasewasementara`);

--
-- Indexes for table `t_ongkir`
--
ALTER TABLE `t_ongkir`
 ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `t_penyewa`
--
ALTER TABLE `t_penyewa`
 ADD PRIMARY KEY (`kode_penyewa`);

--
-- Indexes for table `t_timesheet`
--
ALTER TABLE `t_timesheet`
 ADD PRIMARY KEY (`kode_alat`), ADD UNIQUE KEY `no_nota` (`no_nota`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_nota_sewa_temp`
--
ALTER TABLE `t_nota_sewa_temp`
MODIFY `no_notasewasementara` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_nota_sewa`
--
ALTER TABLE `t_nota_sewa`
ADD CONSTRAINT `t_nota_sewa_ibfk_2` FOREIGN KEY (`kode_penyewa`) REFERENCES `t_penyewa` (`kode_penyewa`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
